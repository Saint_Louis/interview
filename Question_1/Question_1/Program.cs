﻿using System;

namespace Question_1 {
    class Program {
        static bool Compare(BinaryTree bt1, BinaryTree bt2) {
            if(bt1 == null && bt2 == null) {
                return true;
            }else if(bt1 == null || bt2 == null) {
                return false;
            }
            else {
                return _CompareTreeNode(bt1.rootNode, bt2.rootNode);
            }
        }

        static bool _CompareTreeNode(BinaryTreeNode btn1, BinaryTreeNode btn2) {
            if(btn1 == null && btn2 == null) {
                return true;
            }
            else if(btn1 == null || btn2 == null) {
                return false;
            }
            else if(btn1.val == btn2.val) {
                return _CompareTreeNode(btn1.left, btn2.left) && _CompareTreeNode(btn1.right, btn2.right);
            }
            else {
                return false;
            }
        }

        static void Main(string[] args) {
            //初始化测试数据
            string treeStr = "51,7,13,5,,26,17,,,,,9";
            BinaryTree bt = new BinaryTree();
            bt.rootNode = new BinaryTreeNode(51, new BinaryTreeNode(7, new BinaryTreeNode(5)), new BinaryTreeNode(13, new BinaryTreeNode(26, new BinaryTreeNode(9)), new BinaryTreeNode(17)));
            
            Solution sl = new Solution();
            var solutionTree = sl.Deserialize(treeStr);
            var solutionStr = sl.Serialize(bt);

            Console.WriteLine((Compare(solutionTree, bt) && treeStr == solutionStr) ? "测试通过" : "测试失败");
        }
    }
}
