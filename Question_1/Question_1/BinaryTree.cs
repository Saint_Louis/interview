﻿namespace Question_1 {
    class BinaryTreeNode {
        public BinaryTreeNode left;
        public BinaryTreeNode right;
        public int val;

        public BinaryTreeNode(int _val = 0, BinaryTreeNode _left = null, BinaryTreeNode _right = null) {
            left = _left;
            right = _right;
            val = _val;
        }
    }

    class BinaryTree {
        public BinaryTreeNode rootNode;
    }

    class Solution {
        public string Serialize(BinaryTree bt) {
            //TODO 补充功能
            return "";
        }
    
        public BinaryTree Deserialize(string str) {
            //TODO 补充功能
            return null;
        }
    }
}
